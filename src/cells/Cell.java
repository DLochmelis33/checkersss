package cells;

import board.Coordinates;
import pieces.Piece;

public class Cell {
	final Coordinates coordinates;
	public Piece piece = null;

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public Cell(Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	public Piece getPiece() {
		return piece;
	}

	public void setPiece(Piece piece) {
		this.piece = piece;
	}

	public Cell(Coordinates coordinates, Piece piece) {
		this.coordinates = coordinates;
		this.piece = piece;
	}

}
