package board;

public class Coordinates {
	int row;

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	int col;

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public Coordinates(int row, int col) {
		this.row = row;
		this.col = col;
	}

}
