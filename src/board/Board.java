package board;

import cells.Cell;

public class Board {

	public static Cell[][] cells = new Cell[8][8];

	public Board() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				cells[i][j] = new Cell(new Coordinates(i, j));
			}
		}
	}

}
