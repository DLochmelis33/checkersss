package controllers;

import board.Board;
import board.Coordinates;
import board.TmpBoard;
import cells.Cell;
import pieces.Checker;
import pieces.King;
import pieces.Piece;

public class Controller {

	public static boolean hasEaten;

	public static void prepareBoard() {
		int[] columns026 = { 0, 2, 4, 6 };
		int[] columns157 = { 1, 3, 5, 7 };

		int[] rows = { 0, 1, 2, 5, 6, 7 };

		for (int i : rows) {
			if (i % 2 == 0) {
				for (int j : columns026) {
					if (i == 0 || i == 1 || i == 2) {
						Board.cells[i][j].piece = new Checker(new Coordinates(i, j), true);
					} else {
						Board.cells[i][j].piece = new Checker(new Coordinates(i, j), false);
					}
				}
			} else {
				for (int j : columns157) {
					if (i == 0 || i == 1 || i == 2) {
						Board.cells[i][j].piece = new Checker(new Coordinates(i, j), true);
					} else {
						Board.cells[i][j].piece = new Checker(new Coordinates(i, j), false);
					}
				}
			}
		}
	}

	public static void turnCheckersIntoKings() {
		for (int i = 0; i < 7; i++) {
			if (Board.cells[0][i].piece == null) {
				continue;
			}
			if (Board.cells[0][i].piece.getClass().getSimpleName().equals("Checker")
					&& !Board.cells[0][i].piece.isWhite) {
				Board.cells[0][i].piece.turnIntoKing();
			}

		}
		for (int i = 0; i < 8; i++) {
			if (Board.cells[7][i].piece == null) {
				continue;
			}
			if (Board.cells[7][i].piece.getClass().getSimpleName().equals("Checker")
					&& Board.cells[7][i].piece.isWhite) {
				Board.cells[7][i].piece.turnIntoKing();
			}
		}
	}

	public static void waitForValidMove() {
		// waits for a move, checks it's validity and does it if everything is
		// right

		String s = Main.sc.next();

		boolean noErrors = false;
		hasEaten = false;

		// matching command

		if (s.matches("[abcdefgh][12345678](-[abcdefgh][12345678])+")) {
			char[] arr = s.toCharArray();

			TmpBoard tmpb = new TmpBoard();

			// it is almost impossible to make right checking without such
			// copying as we need to check each move, but without moving piece
			// we can't invoke needed methods

			copyTo(tmpb);

			int movesAmount = (arr.length - 2) / 3;

			for (int i = 0; i < movesAmount; i++) {

				// based on char number

				int sourceCol = (int) arr[i * 3] - 97;
				int sourceRow = (int) arr[i * 3 + 1] - 48 - 1;

				if (Board.cells[sourceRow][sourceCol].piece == null) {
					throw new IllegalArgumentException("Invalid move: no piece to move!");
				}

				// checking turn consequence

				if (Main.isWhitesTurn != Board.cells[sourceRow][sourceCol].piece.isWhite) {
					throw new IllegalArgumentException(
							"Invalid move: it is " + (Main.isWhitesTurn == true ? "White's " : "Black's ") + "move!");
				}

				int targetCol = (int) arr[i * 3 + 3] - 97;
				int targetRow = (int) arr[i * 3 + 4] - 48 - 1;

				// to avoid NPE
				Board.cells[sourceRow][sourceCol].piece.preliminaryValidityCheck(Board.cells[targetRow][targetCol]);

				if (movesAmount > 1 && !Board.cells[sourceRow][sourceCol].piece
						.doesMoveNeedEating(Board.cells[targetRow][targetCol])) {
					throw new IllegalArgumentException("Invalid move: only eating is possible in a row!");
				}

				// checking if eating is possible

				Board.cells[sourceRow][sourceCol].piece.move(Board.cells[targetRow][targetCol]);
			}

			// actually moving

			turnCheckersIntoKings();

			noErrors = true;

		}

		if (s.matches("DRAWBOARD") || s.matches("drawboard")) {
			View.drawBoard();
			noErrors = true;
			waitForValidMove();
		}
		// beginning of admin commands

		if (s.matches("@@@NEWCHECKER[abcdefgh][12345678][WB]")) {
			char[] arr = s.toCharArray();
			int col = (int) arr[13] - 97;
			int row = (int) arr[14] - 48 - 1;
			boolean isWhite = (arr[15] == 'W' ? true : false);

			Board.cells[row][col].piece = new Checker(new Coordinates(row, col), isWhite);
			System.err.println("// created a new checker //");
			noErrors = true;
		}

		if (s.matches("@@@SWAPTURN")) {
			Main.isWhitesTurn = !Main.isWhitesTurn;
			System.err.println(
					"// swapped  turn -  now it is " + (Main.isWhitesTurn ? "White's " : "Black's ") + "turn //");
			noErrors = true;
		}

		if (s.matches("@@@NEWKING[abcdefgh][12345678][WB]")) {
			char[] arr = s.toCharArray();
			int col = (int) arr[10] - 97;
			int row = (int) arr[11] - 48 - 1;
			boolean isWhite = (arr[12] == 'W' ? true : false);

			Board.cells[row][col].piece = null;
			Board.cells[row][col].piece = new King(new Coordinates(row, col), isWhite);
			System.err.println("// created a new king //");
			noErrors = true;
		}

		if (s.matches("@@@NEWCHECKERARRAY([abcdefgh][12345678][WB])+")) {
			char[] arr = s.toCharArray();

			int amountOfCheckers = (arr.length - 18) / 3;

			for (int i = 0; i < amountOfCheckers; i++) {
				int col = (int) arr[i * 3 + 18] - 97;
				int row = (int) arr[i * 3 + 18 + 1] - 48 - 1;

				Board.cells[row][col].piece = new Checker(new Coordinates(row, col),
						(arr[i * 3 + 18 + 2] == 'W' ? true : false));
			}

			System.err.print("// created an array of checkers //");
			noErrors = true;

		}

		if (s.matches("@@@MOVE[abcdefgh][12345678]TO[abcdefgh][12345678]")) {
			char[] arr = s.toCharArray();
			int sourceCol = (int) arr[7] - 97;
			int sourceRow = (int) arr[8] - 48 - 1;

			if (Board.cells[sourceRow][sourceCol].piece == null) {
				throw new IllegalArgumentException("// invalid move: no piece to move //");
			}

			int targetCol = (int) arr[11] - 97;
			int targetRow = (int) arr[12] - 48 - 1;

			Board.cells[sourceRow][sourceCol].piece.actualMove(Board.cells[targetRow][targetCol]);

			System.err.println("// moved //");
			noErrors = true;

		}

		if (s.matches("@@@PREPAREBOARD")) {
			prepareBoard();
		}

		if (s.matches("@@@CLEAR[abcdefgh][12345678]")) {
			char[] arr = s.toCharArray();
			int col = (int) arr[8] - 97;
			int row = (int) arr[9] - 48 - 1;

			Board.cells[row][col].piece = null;
			System.err.println("// cleared a cell //");
			noErrors = true;
		}

		if (s.matches("@@@CLEARBOARD")) {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					Board.cells[i][j].piece = null;
				}
			}
			System.err.println("// cleared the board //");
			noErrors = true;
		}

		if (s.matches("@@@GAMEOVER")) {
			Main.isGameOver = true;
			noErrors = true;
		}

		if (s.matches("@@@STOP")) {
			System.err.println("// terminated //");
			System.exit(0);
		}

		// end of admin commands

		if (!noErrors) {
			throw new IllegalArgumentException("Invalid move: wrong command!");
		}

	}

	public static void copyTo(TmpBoard tmpb) {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				tmpb.cells[i][j] = Board.cells[i][j];
			}
		}
	}

	public static void copyFrom(TmpBoard tmpb) {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				Board.cells[i][j] = tmpb.cells[i][j];
			}
		}
	}

	public static boolean checkIfWhiteCanMove() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (Board.cells[i][j].piece == null || !Board.cells[i][j].piece.isWhite) {
					continue;
				}
				if (Board.cells[i][j].piece.canMove()) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean checkIfBlackCanMove() {
		TmpBoard tmpb = new TmpBoard();
		copyTo(tmpb);
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (Board.cells[i][j].piece == null || Board.cells[i][j].piece.isWhite) {
					continue;
				}
				if (Board.cells[i][j].piece.canMove()) {
					copyFrom(tmpb);
					return true;
				}
			}
		}
		copyFrom(tmpb);
		return false;
	}

	public static boolean checkIfWhiteCanEat() {
		TmpBoard tmpb = new TmpBoard();
		copyTo(tmpb);
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (Board.cells[i][j].piece == null || !Board.cells[i][j].piece.isWhite) {
					continue;
				}
				if (Board.cells[i][j].piece.canEat()) {
					copyFrom(tmpb);
					return true;
				}
			}
		}
		copyFrom(tmpb);
		return false;
	}

	public static boolean checkIfBlackCanEat() {
		TmpBoard tmpb = new TmpBoard();
		copyTo(tmpb);
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (Board.cells[i][j].piece == null || Board.cells[i][j].piece.isWhite) {
					continue;
				}
				if (Board.cells[i][j].piece.canEat()) {
					copyFrom(tmpb);
					return true;
				}
			}
		}
		copyFrom(tmpb);
		return false;

	}

}
