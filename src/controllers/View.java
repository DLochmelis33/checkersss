package controllers;

import board.Board;


public class View {

	static void newLine() {
		System.out.print("     ");
		for (int i = 0; i < 49; i++) {
			System.out.print("#");
		}
		System.out.println();
	}

	public static void drawBoard() {

		newLine();
		for (int i = 7; i >= 0; i--) {
			System.out.println("     #     #     #     #     #     #     #     #     #");
			System.out.print("  " + (i + 1) + "  #");
			for (int j = 0; j < 8; j++) {
				if (Board.cells[i][j].piece == null) {
					System.out.print("     #");
					continue;
				}
				if (Board.cells[i][j].piece.isWhite) {
					if (Board.cells[i][j].piece.getClass().getSimpleName().equals("King")) {
						System.out.print(" WWW #");
					} else {
						System.out.print("  W  #");
					}
				} else {
					if (Board.cells[i][j].piece.getClass().getSimpleName().equals("King")) {
						System.out.print(" BBB #");
					} else {
						System.out.print("  B  #");
					}

				}
			}
			System.out.println();
			System.out.println("     #     #     #     #     #     #     #     #     #");
			newLine();
		}
		System.out.println("\n        a     b     c     d     e     f     g     h");
		System.out.println("\n----------------------------------------------------------\n");
	}

}
