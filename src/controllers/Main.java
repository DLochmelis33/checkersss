package controllers;

import java.util.Scanner;
import board.Board;

public class Main {

	static Scanner sc = new Scanner(System.in);

	static boolean isGameOver = false;

	static boolean isWhitesTurn = true;

	public static void swapTurn() {
		isWhitesTurn = !isWhitesTurn;
	}

	public static void main(String[] args) {

		@SuppressWarnings("unused")
		Board b = new Board();

		Controller.prepareBoard();

		View.drawBoard();

		while (!isGameOver) {
			try{
			Controller.waitForValidMove();
			}catch(IllegalArgumentException e){
				System.err.print(e.getMessage());
				System.out.println("\nMake your move again.\n");
				continue;
			}
			View.drawBoard();
			swapTurn();
			
			// NEED TO DEAL WITH NPE FIRST

			// if (isWhitesTurn ? !Controller.checkWhiteMovingPossibility() :
			// !Controller.checkBlackMovingPossibility()) {
			// isGameOver = true;
			// }

		}

		System.out.println("Game over!\n" + "Winner is " + (isWhitesTurn ? "White" : "Black") + "!");

	}

}
