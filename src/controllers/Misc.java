package controllers;

public class Misc {

	public static int extractCol(char c) {
		int sourceCol = -1;
		switch (c) {
		case 'a':
			sourceCol = 0;
		case 'b':
			sourceCol = 1;
		case 'c':
			sourceCol = 2;
		case 'd':
			sourceCol = 3;
		case 'e':
			sourceCol = 4;
		case 'f':
			sourceCol = 5;
		case 'g':
			sourceCol = 6;
		case 'h':
			sourceCol = 7;
		}
		return sourceCol;
	}

}
