package pieces;

import board.Coordinates;
import cells.Cell;

public class Checker extends Piece {

	public Checker(Coordinates coordinates, boolean isWhite) {
		super(coordinates, isWhite);
	}

	@Override
	public void move(Cell target) {
		Cell source = new Cell(this.coordinates);

		int deltaCol = source.getCoordinates().getCol() - target.getCoordinates().getCol();
		int deltaRow = source.getCoordinates().getRow() - target.getCoordinates().getRow();
		
		// see exception messages as comments

		if (Math.abs(deltaRow) != Math.abs(deltaCol)) {
			throw new IllegalArgumentException("Invalid move: not diagonal move!");
		}

		if (Math.abs(deltaRow) > 2) {
			throw new IllegalArgumentException("Invalid move: too far!");
		}

		if (target.getPiece() != null) {
			throw new IllegalArgumentException("Invalid move: target cell is occupied!");
		}

		if ((deltaRow == 1 && this.isWhite) || (deltaRow == -1 && !this.isWhite)) {
			throw new IllegalArgumentException("Invalid move: cannot move backwards!");
		}

		if (deltaRow == -1 || deltaRow == 1) {
			makeMove(target);
		} else {
			Cell toEat = new Cell(new Coordinates(this.coordinates.getRow() + (deltaRow == 2 ? -1 : 1),
					this.coordinates.getCol() + (deltaCol == 2 ? -1 : 1)));
			makeMove(target, toEat);
		}

	}

}
