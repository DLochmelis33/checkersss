package pieces;

import board.Coordinates;
import board.TmpBoard;
import board.Board;
import cells.Cell;
import controllers.Controller;

public abstract class Piece {

	Coordinates coordinates;
	public final boolean isWhite;

	public Piece(Coordinates coordinates, boolean isWhite) {
		this.coordinates = coordinates;
		this.isWhite = isWhite;
	}

	public Coordinates getCoordinates() {
		return this.coordinates;
	}

	public void move(Cell target) {
	};

	public void actualMove(Cell target) {
		Board.cells[this.coordinates.getRow()][this.coordinates.getCol()].piece = null;
		this.coordinates = target.getCoordinates();
		Board.cells[target.getCoordinates().getRow()][target.getCoordinates().getCol()].piece = this;
	}

	public void makeMove(Cell toGo, Cell toEat) {

		if (Board.cells[toEat.getCoordinates().getRow()][toEat.getCoordinates().getCol()].piece == null) {
			throw new IllegalArgumentException("Invalid move: no piece(s) to eat!");
		}
		if (Board.cells[toEat.getCoordinates().getRow()][toEat.getCoordinates()
				.getCol()].piece.isWhite == this.isWhite) {
			throw new IllegalArgumentException("Invalid move: cannot eat self-colour piece!");
		}

		Board.cells[toEat.getCoordinates().getRow()][toEat.getCoordinates().getCol()].piece = null;

		this.actualMove(toGo);

	}

	public void makeMove(Cell toGo) {
		this.actualMove(toGo);
	}

	public void preliminaryValidityCheck(Cell target) {

		Cell source = new Cell(this.coordinates);
		
		// see exception messages as comments
		
		int deltaCol = source.getCoordinates().getCol() - target.getCoordinates().getCol();
		int deltaRow = source.getCoordinates().getRow() - target.getCoordinates().getRow();

		if (Math.abs(deltaRow) != Math.abs(deltaCol)) {
			throw new IllegalArgumentException("Invalid move: not diagonal move!");
		}

		if (deltaRow == 0) {
			throw new IllegalArgumentException("Invalid move: ...what did you mean?");
		}

		if (target.getPiece() != null) {
			throw new IllegalArgumentException("Invalid move: target cell is occupied!");
		}

	}

	public boolean doesMoveNeedEating(Cell target) {
		int deltaRow = this.getCoordinates().getRow() - target.getCoordinates().getRow();
		int deltaCol = this.getCoordinates().getCol() - target.getCoordinates().getCol();

		for (int i = 0; i < Math.abs(deltaRow) - 1; i++) {
			Cell tmp = Board.cells[this.getCoordinates().getRow() - (deltaRow > 0 ? i : -i)][this.getCoordinates()
					.getCol() - (deltaCol > 0 ? i : -i)];

			if (tmp.piece == null) {
				continue;
			}
			return true;
		}
		return false;
	}

	public void turnIntoKing() {
		Board.cells[this.coordinates.getRow()][this.coordinates.getCol()].piece = new King(
				new Coordinates(this.coordinates.getRow(), this.coordinates.getCol()), this.isWhite);
	}

	public boolean checkMoveValidity(Cell target) {
		return false;
	}

	public boolean canMove() {
		TmpBoard tmpb = new TmpBoard();
		Controller.copyTo(tmpb);

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				try {
					preliminaryValidityCheck(Board.cells[i][j]);
					move(Board.cells[i][j]);
					// if we reached here, turn is possible
					return true;
				} catch (IllegalArgumentException e) {
				}
				Controller.copyFrom(tmpb);
			}
		}
		return false;
	}

	public boolean canEat() {
		TmpBoard tmpb = new TmpBoard();
		Controller.copyTo(tmpb);

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				try {
					preliminaryValidityCheck(Board.cells[i][j]);
					if (doesMoveNeedEating(Board.cells[i][j])) {
						move(Board.cells[i][j]);
						// for now move is possible and requires eating
						Controller.copyFrom(tmpb);
						return true;
					}
				} catch (IllegalArgumentException e) {
					Controller.copyFrom(tmpb);
				}
			}
		}
		Controller.copyFrom(tmpb);
		return false;
	}

}
