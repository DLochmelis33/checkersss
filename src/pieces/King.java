package pieces;

import board.Board;
import board.Coordinates;
import cells.Cell;

public class King extends Piece {

	public King(Coordinates coordinates, boolean isWhite) {
		super(coordinates, isWhite);
	}

	@Override
	public void move(Cell target) {
		Cell source = new Cell(this.coordinates);

		int deltaCol = source.getCoordinates().getCol() - target.getCoordinates().getCol();
		int deltaRow = source.getCoordinates().getRow() - target.getCoordinates().getRow();

		// see exception messages as comments

		if (Math.abs(deltaRow) != Math.abs(deltaCol)) {
			throw new IllegalArgumentException("Invalid move: not diagonal move!");
		}

		if (target.getPiece() != null) {
			throw new IllegalArgumentException("Invalid move: target cell is occupied!");
		}

		// does the piece eat anything

		if (Math.abs(deltaRow) == 1) {
			makeMove(target);
		} else {
			Cell toEat = null;
			for (int i = 1; i < Math.abs(deltaRow); i++) {
				Cell tmp = Board.cells[source.getCoordinates().getRow() - (deltaRow > 0 ? i
						: -i)][source.getCoordinates().getCol() - (deltaCol > 0 ? i : -i)];

				if (tmp.piece == null) {
					continue;
				}
				if (toEat != null) {
					throw new IllegalArgumentException("Invalid move: cannot eat more than one piece at once!");
				}

				toEat = tmp;
			}

			if (toEat == null) {
				makeMove(target);
			} else {
				makeMove(target, toEat);
			}
		}

	}

}
